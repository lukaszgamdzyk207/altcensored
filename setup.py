from setuptools import setup

setup(
    name='alt2',
    packages=['alt2'],
    include_package_data=True,
    install_requires=[
        'flask',
    ],
)