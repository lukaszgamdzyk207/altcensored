��    9      �              �     �     �     �     �     �     �     �             �         �  0   �     �  -   �     '     ,     5     =     B  	   G     Q     ^     q     v     |     �     �     �     �     �  !   �     �     �  &   �  �     8   �     �     �     �       #        ?  ?   F     �     �     �     �     �     �     �     �     �     �     �       Z     �  p     �	     
     
  	   $
     .
     5
     R
  
   Z
     e
  ~   x
  #   �
  3        O  4   T     �     �     �     �     �     �     �     �     �     �     �     �     	            	   !     +     K     N  -   f  �   �  C   _  
   �     �     �     �  -   �  	     C        \     d     ~     �     �     �     �  	   �  
   �     �     �  	   �  [   �   "Limited state" videos About Archive Category Channel Community Guidelines Created Deleted Download Torrent Download videos with "right mouse button click" inside the video window or use the "Download Torrent" link. Channel and Video RSS Feeds Email new channel suggestions to Embed videos in any website exactly like YouTube False Find videos and channels using the search bar Full Language Limited None Part Published Published on Released under the Save Stats Subscribers Terms and Conditions Total True View on Views access to videos that are neither and arbitrarily enforced are being archived in case of deletion are placed behind a warning message, cannot be shared, monetized or easily found, and have likes, comments, view counts and suggested videos disabled contain torrent links to easily download multiple videos deleted ever-changing have been deleted and illegal is an unbiased community catalog of latest limited state, removed, and self-censored YouTube videos across limits monitored channels, of which most limited newest nor violate their own oldest or popular removes search videos that violate their views while Creators increasingly "self-censor" trying to avoid channel strikes and terminations Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2020-11-02 19:39-0300
PO-Revision-Date: 2020-07-27 23:24+0000
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: nl
Language-Team: nl <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.8.0
 "Beperkte staat" video's Over Archief Categorie Kanaal de communautaire richtlijnen Gemaakt Verwijderd Torrent downloaden Download video's met de rechtermuisknop in het videovenster of gebruik de "Torrent downloaden" link. Kanaal en video RSS-feeds E-mail nieuwe kanaalsuggesties naar Embed video's in elke website precies zoals YouTube Vals Video's en kanalen vinden met behulp van de zoekbalk Vol Taal Beperkt Geen Deel Gepubliceerd Gepubliceerd op Vrijgegeven onder de Bespaar Statistieken Abonnees Algemene Voorwaarden Totaal Waar Aanzicht op Weergaven de toegang tot video's die noch en willekeurig afgedwongen worden gearchiveerd in geval van verwijdering worden geplaatst achter een waarschuwingsbericht, kunnen niet worden gedeeld, te gelde gemaakt of gemakkelijk te vinden, en hebben graag, commentaar, bekijken telt en gesuggereerde video's uitgeschakeld bevatten torrentlinks om gemakkelijk meerdere video's te downloaden verwijderd steeds wisselende zijn geschrapt en illegaal zijn is een onpartijdige gemeenschapscatalogus van recentste beperkte staat, verwijderd, en zelfcensureerde YouTube-video's over beperkt bewaakte kanalen, waarvan meest beperkt nieuwste noch in strijd met hun eigen oudste of populaire verwijdert zoek video's die hun weergaven terwijl de makers steeds meer zelfcensuur proberen te vermijden stakingen en beëindigingen 