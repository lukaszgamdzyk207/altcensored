# Spanish translations for PROJECT.
# Copyright (C) 2020 ORGANIZATION
# This file is distributed under the same license as the PROJECT project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2020-11-02 19:39-0300\n"
"PO-Revision-Date: 2020-06-24 15:39+0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language: es\n"
"Language-Team: es <LL@li.org>\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"

#: auth.py:116
msgid "User does not exist"
msgstr "El usuario no existe"

#: auth.py:120
msgid "Reset password email sent"
msgstr "Restablecer la contraseña enviada por correo electrónico"

#: auth.py:131
msgid "Username exists"
msgstr "Usuario existe"

#: auth.py:139
msgid "Captcha not correct"
msgstr "Captcha no es correcto"

#: auth.py:148
msgid "Email taken"
msgstr "Correo electrónico tomado"

#: auth.py:157
msgid "Confirmation email sent"
msgstr "Email de confirmación enviado"

#: auth.py:180
msgid "Account not verified. Confirmation email resent"
msgstr "Cuenta no verificada. Email de confirmación reenviado"

#: auth.py:186
msgid "Email and password combination is invalid"
msgstr "Combinación de Email y contraseña es inválida"

#: auth.py:196
msgid "The confirmation link is invalid or has expired"
msgstr "Enlace de confirmación no es válido o ha caducado"

#: auth.py:203
msgid "Account already confirmed. Please login"
msgstr "Cuenta ya confirmada. Por favor, entra"

#: auth.py:215
msgid "Thank-you for confirming your account"
msgstr "Gracias por confirmar su cuenta"

#: auth.py:224
msgid "The reset password link is invalid or has expired"
msgstr "El enlace para restablecer la contraseña es inválido o ha caducado"

#: auth.py:233
msgid "Password has been updated"
msgstr "La contraseña ha sido actualizada"

#: auth.py:263
msgid "Delete User "
msgstr "Borrar Usuario "

#: playlist.py:18 settings.py:112
msgid "Profanity forbidden"
msgstr "Se prohíbe la blasfemia"

#: playlist.py:19
msgid "Title exists"
msgstr "El título existe"

#: playlist.py:289
msgid "Remove Playlist"
msgstr "Eliminar Playlist"

#: user.py:112 user.py:135
msgid "History Empty"
msgstr "Historia Vacío"

#: user.py:138
msgid "Clear History"
msgstr "Borrar Historia"

#: user.py:147
msgid "History Cleared"
msgstr "Historia Borrada"

#: user.py:150
msgid "History Not Cleared"
msgstr "Historia No Borrada"

#: user.py:177
msgid "No WatchLater Available"
msgstr "No Hay VerDespues"

#: user.py:234
msgid "WatchLater Empty"
msgstr "VerDespues Vacío"

#: user.py:237
msgid "Clear WatchLater"
msgstr "Borrar VerDespues"

#: user.py:246
msgid "WatchLater Cleared"
msgstr "VerDespues Borrada"

#: user.py:251
msgid "WatchLater Not Cleared"
msgstr "VerDespues No Borrada"

#: templates/base.html:63 templates/widgets/playlist.html:104
msgid "search"
msgstr "buscar"

#: templates/base.html:91
msgid "Log In"
msgstr "Entrar"

#: templates/base.html:95
msgid "Log Out"
msgstr "Salir"

#: templates/base.html:160
msgid "Released under the"
msgstr "Lanzado bajo el"

#: templates/base.html:166
msgid "About"
msgstr "Sobre"

#: templates/base.html:169
msgid "Privacy"
msgstr "Privacidad"

#: templates/base.html:172
msgid "FAQ"
msgstr ""

#: templates/about/about_index.html:32
msgid "limits"
msgstr "limita"

#: templates/about/about_index.html:33
msgid "access to videos that are neither"
msgstr "el acceso a las videos que no son"

#: templates/about/about_index.html:35
msgid "illegal"
msgstr "ilegal"

#: templates/about/about_index.html:36
msgid "nor violate their own"
msgstr "ni violar la suya"

#: templates/about/about_index.html:38
msgid "Terms and Conditions"
msgstr "Términos y Condiciones"

#: templates/about/about_index.html:38
msgid "or"
msgstr "o"

#: templates/about/about_index.html:40 templates/about/about_index.html:55
msgid "Community Guidelines"
msgstr "Principios de la Comunidad"

#: templates/about/about_index.html:43
msgid "\"Limited state\" videos"
msgstr "Videos de \"estado limitado\""

#: templates/about/about_index.html:44
msgid ""
"are placed behind a warning message, cannot be shared, monetized or "
"easily found, and have likes, comments, view counts and suggested videos "
"disabled"
msgstr ""
"se colocan detrás de un mensaje de advertencia, no se pueden compartir, "
"monetizar o encontrar fácilmente, y likes, comentarios, recuentos de "
"vistas y videos sugeridos estan dishabilitados"

#: templates/about/about_index.html:49
msgid "removes"
msgstr "elimina"

#: templates/about/about_index.html:50
msgid "videos that violate their"
msgstr "videos que violan sus"

#: templates/about/about_index.html:52
msgid "ever-changing"
msgstr "siempre cambiante"

#: templates/about/about_index.html:52
msgid "and"
msgstr "y"

#: templates/about/about_index.html:54
msgid "arbitrarily enforced"
msgstr "aplicado arbitrariamente"

#: templates/about/about_index.html:56
msgid ""
"while Creators increasingly \"self-censor\" trying to avoid channel "
"strikes and terminations"
msgstr ""
"mientras que los creadores se autocensuran cada vez más tratando de "
"evitar las penalizaciones y las terminaciones de los canales"

#: templates/about/about_index.html:60
msgid "is an unbiased community catalog of"
msgstr "es un catálogo comunitario imparcial de"

#: templates/about/about_index.html:63
msgid "limited state, removed, and self-censored YouTube videos across"
msgstr ""
"videos de YouTube de estado limitado, eliminados y autocensurados a "
"través de"

#: templates/about/about_index.html:66
msgid "monitored channels, of which"
msgstr "canales vigilados, de los cuales"

#: templates/about/about_index.html:69
msgid "have been deleted and"
msgstr "han sido eliminados y"

#: templates/about/about_index.html:72
msgid "are being archived in case of deletion"
msgstr "se archivan en caso de eliminación"

#: templates/about/about_index.html:75
msgid "Find videos and channels using the search bar"
msgstr "Encuentra videos y canales usando la barra de búsqueda"

#: templates/about/about_index.html:76
msgid ""
"Download videos with \"right mouse button click\" inside the video window"
" or use the \"Download Torrent\" link. Channel and Video RSS Feeds"
msgstr ""
"Descargue videos haciendo clic con el botón derecho del mouse dentro de "
"la ventana de video o use el enlace \"Descargar Torrent\". Canal y Video "
"RSS Feeds"

#: templates/about/about_index.html:80
msgid "contain torrent links to easily download multiple videos"
msgstr "contienen enlaces de torrent para descargar fácilmente múltiples videos"

#: templates/about/about_index.html:81
msgid "Embed videos in any website exactly like YouTube"
msgstr "Embed videos en cualquier sitio web exactamente como YouTube"

#: templates/about/about_index.html:84
msgid "Email new channel suggestions to"
msgstr "Enviar sugerencias de canales nuevos a"

#: templates/about/about_index.html:89
msgid "Privacy Policy"
msgstr "Política de privacidad"

#: templates/about/about_index.html:93
msgid "Data you directly provide"
msgstr "Datos que proporcionas directamente"

#: templates/about/about_index.html:94
msgid ""
"Data that you provide to the website for the purpose of its operation "
"(e.g., an account name or password, or playlist) will be stored in the "
"websites database until the user decides to remove it. This data will not"
" be intentionally shared with anyone or anything"
msgstr ""
"Los datos que proporcione al sitio web con el fin de su funcionamiento "
"(por ejemplo, un nombre de cuenta o una contraseña, o una lista de "
"reproducción) se almacenarán en la base de datos de los sitios web hasta "
"que el usuario decida eliminarlos. Estos datos no se compartirse intencionalmente con "
"cualquier persona o cosa"

#: templates/about/about_index.html:96
msgid "Information stored about a registered user is limited to"
msgstr "La información almacenada sobre un usuario registrado se limita a"

#: templates/about/about_index.html:98
msgid ""
"a user ID (for persistent storage of preferences and playlists) and "
"hashed password"
msgstr ""
"una identificación de usuario (para el almacenamiento permanente de "
"preferencias y listas de reproducción) y una contraseña con hash"

#: templates/about/about_index.html:99
msgid "a json object containing user preferences"
msgstr "un objeto json que contiene las preferencias del usuario"

#: templates/about/about_index.html:100
msgid "a list of video IDs identifying watched videos"
msgstr "una lista de ID de video que identifican videos vistos"

#: templates/about/about_index.html:102
msgid "Users can clear their watch history using the "
msgstr "Los usuarios pueden borrar su historial de reproducciones mediante el "

#: templates/about/about_index.html:102
msgid "clear watch history"
msgstr "borrar historial de reproducciones"

#: templates/about/about_index.html:104
msgid "Data you passively provide"
msgstr "Datos que proporciona pasivamente"

#: templates/about/about_index.html:105
msgid ""
"When you request any resource from this website (e.g., a page, font, or "
"image) information about the request may be logged"
msgstr ""
"Cuando solicita cualquier recurso de este sitio web (por ejemplo, una "
"página, fuente o imagen), se puede registrar información sobre la "
"solicitud"

#: templates/about/about_index.html:106
msgid "Information about a request is limited to"
msgstr "La información sobre una solicitud se limita a"

#: templates/about/about_index.html:108
msgid "the time the request was made"
msgstr "la hora en que se hizo la solicitud"

#: templates/about/about_index.html:109
msgid "the status code of the response"
msgstr "el código de estado de la respuesta"

#: templates/about/about_index.html:110
msgid "the method of the request"
msgstr "el método de la solicitud"

#: templates/about/about_index.html:111
msgid "the requested URL"
msgstr "la URL solicitada"

#: templates/about/about_index.html:112
msgid "how long it took to complete the request"
msgstr "cuánto tiempo se tardó en completar la solicitud"

#: templates/about/about_index.html:115
msgid ""
"No identifying information is logged, such as the visitors cookie, user-"
"agent, or full IP address (only the  first /24 bits are stored for "
"geographical feedback). Here are a few lines as an example"
msgstr ""
"No se registra información de identificación, como la cookie de "
"visitantes, el agente de usuario o la dirección IP completa (solo se "
"almacenan los primeros / 24 bits para comentarios geográficos). Aquí hay "
"algunas líneas como ejemplo"

#: templates/about/about_index.html:119
msgid ""
"This website does not store the visitors user-agent or full IP address "
"and does not use fingerprinting, advertisements, or tracking of any form"
msgstr ""
"Este sitio web no almacena el agente de usuario de los visitantes ni la "
"dirección IP completa y no utiliza huellas dactilares, anuncios ni "
"seguimiento de ningún tipo"

#: templates/about/about_index.html:122
msgid "Data stored in your browser"
msgstr "Datos almacenados en su navegador"

#: templates/about/about_index.html:123
msgid ""
"This website uses browser cookies to authenticate registered users. This "
"data consists of"
msgstr ""
"Este sitio web utiliza cookies del navegador para autenticar a los "
"usuarios registrados. Estos datos consisten en"

#: templates/about/about_index.html:125
msgid ""
"An account token to keep you logged into the website between visits, "
"which is sent when any page is loaded while you are logged in"
msgstr ""
"Un token de cuenta para mantenerlo conectado al sitio web entre "
"visitas\"que se envía cuando se carga cualquier página mientras está "
"conectado"

#: templates/about/about_index.html:127
msgid ""
"This website also provides an option to store site preferences (e.g., "
"theme or locale) without an account. Using this feature will store a "
"cookie in the visitors browser containing their preferences. This cookie "
"is sent on every request and does not contain any identifying information"
msgstr ""
"Este sitio web también ofrece una opción para almacenar las preferencias "
"del sitio (por ejemplo,tema o configuración regional) sin una cuenta. El "
"uso de esta función almacenará una cookie en el navegador de los "
"visitantes que contiene sus preferencias. Esta cookie se envía con cada "
"solicitud y no contiene ninguna información de identificación"

#: templates/about/about_index.html:129
msgid ""
"You can remove this data from your browser by logging out of this "
"website, or by using your browsers cookie-related controls to delete the "
"data"
msgstr ""
"Puede eliminar estos datos de su navegador cerrando sesión en este sitio "
"web, o utilizando los controles relacionados con las cookies de su "
"navegador para eliminar los datos"

#: templates/about/about_index.html:131
msgid "Removal of data"
msgstr "Eliminación de datos"

#: templates/about/about_index.html:132
msgid ""
"To remove data stored in your browser, you can log out of the website, or"
" you can use your browsers cookie-related controls to delete the data"
msgstr ""
"Para eliminar los datos almacenados en su navegador, puede cerrar sesión "
"en el sitio web, o puede usar los controles relacionados con las cookies "
"de su navegador para eliminar los datos"

#: templates/about/about_index.html:133
msgid ""
"To remove data that has been stored in the websites database, you can use"
" the "
msgstr ""
"Para eliminar datos que se han almacenado en la base de datos de sitios "
"web, puede utilizar el "

#: templates/about/about_index.html:134
msgid "delete my account</u></a></b> page"
msgstr "página eliminar mi cuenta </u> </a> </b>"

#: templates/about/about_index.html:138
msgid "Frequently Asked Questions"
msgstr "Preguntas Frecuentes"

#: templates/about/about_index.html:141
msgid "How can I find videos/channels?"
msgstr "¿Cómo puedo encontrar videos / canales?"

#: templates/about/about_index.html:142
msgid ""
"The search bar at the top of every page is the best way to locate "
"channels and videos. Use the minimum number of exact words. YouTube "
"channel and video identifiers can also be used"
msgstr ""
"La barra de búsqueda en la parte superior de cada página es la mejor "
"manera de localizarcanales y videos. Use la cantidad mínima de palabras "
"exactas. También se pueden usar los YouTube identificadores de canal y video"

#: templates/about/about_index.html:149
msgid "How can I upload my videos/channels?"
msgstr "¿Cómo puedo subir mis videos/canales?"

#: templates/about/about_index.html:150
msgid ""
"We receive channel suggestions (preferably through <b>altCensored AT "
"protonmail.com</b>) and add channels that have experienced, or are at-"
"risk of experiencing"
msgstr ""
"Recibimos sugerencias de canales (preferiblemente a través de <b> "
"altCensored AT protonmail.com </b>) y agregamos canales que han "
"experimentado o están en riesgo de experimentar"

#: templates/about/about_index.html:152
msgid "videos placed in "
msgstr "videos colocados en"

#: templates/about/about_index.html:154
msgid "Limited State"
msgstr "Estado limitado"

#: templates/about/about_index.html:154
msgid " or arbitrarily"
msgstr " o arbitrariamente"

#: templates/about/about_index.html:156
msgid "age-restricted"
msgstr "con restricción de edad"

#: templates/about/about_index.html:156
msgid "by"
msgstr "por"

#: templates/about/about_index.html:157
msgid "videos removed or demonetized"
msgstr "videos eliminados o desmonetizados"

#: templates/about/about_index.html:158
msgid "channel demonetization"
msgstr "desmonetización del canal"

#: templates/about/about_index.html:161
msgid "How can I download videos/channels?"
msgstr "¿Cómo puedo descargar videos / canales?"

#: templates/about/about_index.html:163
msgid ""
"Download videos with \"right mouse button click\" inside the video window"
" or use the \"Download Torrent\" link"
msgstr ""
"Descarga videos con \"clic con el botón derecho del mouse \" dentro de la"
" ventana de video o usa el enlace\" Descargar Torrent \\ "

#: templates/about/about_index.html:164
msgid ""
"Channel and Video RSS Feeds contain torrent links to easily download "
"multiple videos with an RSS feed capable torrent client such as"
msgstr ""
"Las fuentes RSS de canales y videos contienen enlaces torrent para "
"descargar fácilmente varios videos con un cliente de torrents compatible "
"con fuentes RSS como"

#: templates/about/about_index.html:171
msgid "How can I embed videos?"
msgstr "¿Cómo puedo insertar videos?"

#: templates/about/about_index.html:172
msgid ""
"Videos can be embedded (with or without iframes) using the same syntax as"
" YouTube"
msgstr ""
"Los videos se pueden insertar (con o sin iframes) usando la misma "
"sintaxis que YouTube"

#: templates/auth/auth_index.html:22 templates/auth/auth_index.html:48
msgid "Email"
msgstr ""

#: templates/auth/auth_index.html:26 templates/auth/auth_index.html:27
#: templates/auth/auth_index.html:52 templates/auth/auth_index.html:53
msgid "Password"
msgstr "Contraseña"

#: templates/auth/auth_index.html:30 templates/auth/auth_index.html:31
#: templates/settings/settings_user_update.html:38
msgid "Username"
msgstr "Nombre de usuario"

#: templates/auth/auth_index.html:39
msgid "Captcha"
msgstr ""

#: templates/auth/auth_index.html:44
msgid "Register"
msgstr "Registrarse"

#: templates/auth/auth_index.html:56
msgid "Sign In/Register"
msgstr "Iniciar sesión/Registrarse"

#: templates/auth/auth_index.html:57
msgid "Reset Password"
msgstr "Restablecer la contraseña"

#: templates/auth/auth_reset_password.html:16
msgid "Video"
msgstr "Vídeo"

#: templates/auth/auth_reset_password.html:21
#: templates/channel/channel_item.html:69
msgid "Channel"
msgstr "Canal"

#: templates/auth/auth_reset_password.html:26
#: templates/video/video_item.html:76 templates/widgets/navtabs.html:72
msgid "Category"
msgstr "Categoría"

#: templates/auth/auth_reset_password.html:50
msgid "New Password"
msgstr "Nueva contraseña"

#: templates/auth/auth_reset_password.html:52
msgid "Update Pasword"
msgstr "Actualiza contraseña"

#: templates/category/category_item.html:47
#: templates/channel/channel_index.html:66
#: templates/language/language_item.html:111
#: templates/language/language_item.html:125
#: templates/language/language_item.html:139
#: templates/language/language_item.html:154
#: templates/video/video_index.html:61 templates/video/video_search.html:126
#: templates/video/video_search.html:141
msgid "latest"
msgstr "último"

#: templates/category/category_item.html:51
#: templates/channel/channel_index.html:70
#: templates/channel/channel_item.html:108
#: templates/language/language_item.html:114
#: templates/language/language_item.html:128
#: templates/language/language_item.html:142
#: templates/language/language_item.html:157
#: templates/playlist/playlist_index.html:30 templates/user/user_index.html:30
#: templates/user/user_playlist_index.html:37
#: templates/video/video_index.html:65 templates/video/video_search.html:130
#: templates/video/video_search.html:145
msgid "newest"
msgstr "más nuevo"

#: templates/category/category_item.html:55
#: templates/channel/channel_index.html:74
#: templates/channel/channel_item.html:112
#: templates/language/language_item.html:120
#: templates/language/language_item.html:134
#: templates/language/language_item.html:148
#: templates/language/language_item.html:163
#: templates/playlist/playlist_index.html:34 templates/user/user_index.html:34
#: templates/user/user_playlist_index.html:41
#: templates/video/video_index.html:69 templates/video/video_search.html:134
#: templates/video/video_search.html:149
msgid "popular"
msgstr ""

#: templates/channel/channel_index.html:41
msgid "deleted"
msgstr "eliminado"

#: templates/channel/channel_index.html:45
msgid "most limited"
msgstr "más limitado"

#: templates/channel/channel_index.html:105
#: templates/channel/channel_item.html:48
#: templates/language/language_item.html:55
#: templates/video/video_search.html:43
msgid "Limited"
msgstr "Limitado"

#: templates/channel/channel_index.html:108
#: templates/language/language_item.html:57
#: templates/video/video_search.html:46
msgid "Total"
msgstr ""

#: templates/channel/channel_index.html:110
#: templates/channel/channel_item.html:50
#: templates/language/language_item.html:58 templates/video/video_item.html:74
#: templates/video/video_search.html:48
msgid "Views"
msgstr "Vistas"

#: templates/channel/channel_index.html:112
#: templates/language/language_item.html:59
#: templates/video/video_search.html:50
msgid "Subs"
msgstr ""

#: templates/channel/channel_index.html:119
#: templates/channel/channel_item.html:54
#: templates/language/language_item.html:67
#: templates/video/video_search.html:57
msgid "Monitor"
msgstr ""

#: templates/channel/channel_index.html:120
#: templates/channel/channel_item.html:56
#: templates/language/language_item.html:68
#: templates/language/language_item.html:72
#: templates/playlist/playlist_item_create_edit.html:49
#: templates/playlist/playlist_item_create_edit.html:51
#: templates/settings/settings_site.html:79
#: templates/settings/settings_site.html:90
#: templates/settings/settings_user_update.html:60
#: templates/video/video_search.html:58
msgid "True"
msgstr "Sí"

#: templates/channel/channel_index.html:121
#: templates/channel/channel_item.html:58
#: templates/language/language_item.html:69
#: templates/language/language_item.html:73
#: templates/playlist/playlist_item_create_edit.html:48
#: templates/playlist/playlist_item_create_edit.html:52
#: templates/settings/settings_site.html:77
#: templates/settings/settings_site.html:88
#: templates/settings/settings_user_update.html:58
#: templates/video/video_search.html:59
msgid "False"
msgstr "No"

#: templates/channel/channel_index.html:123
#: templates/channel/channel_item.html:60
#: templates/language/language_item.html:71
#: templates/video/video_search.html:61
msgid "Archive"
msgstr "Archivar"

#: templates/channel/channel_index.html:124
#: templates/channel/channel_item.html:62 templates/video/video_search.html:62
msgid "Full"
msgstr "Total"

#: templates/channel/channel_index.html:125
#: templates/video/video_search.html:63
msgid "Part"
msgstr "Parte"

#: templates/channel/channel_index.html:126
#: templates/channel/channel_item.html:66 templates/video/video_search.html:64
msgid "None"
msgstr "Nada"

#: templates/channel/channel_index.html:136
#: templates/channel/channel_item.html:72
#: templates/language/language_item.html:83
#: templates/video/video_search.html:75
msgid "Created"
msgstr "Creado"

#: templates/channel/channel_index.html:139
#: templates/channel/channel_item.html:76 templates/video/video_search.html:78
msgid "Deleted"
msgstr "Eliminado"

#: templates/channel/channel_item.html:49
msgid "Videos"
msgstr "Vidéos"

#: templates/channel/channel_item.html:51
msgid "Subscribers"
msgstr "Suscriptores"

#: templates/channel/channel_item.html:64
msgid "Partial"
msgstr ""

#: templates/channel/channel_item.html:70
msgid "Stats"
msgstr ""

#: templates/language/language_item.html:117
#: templates/language/language_item.html:131
#: templates/language/language_item.html:145
#: templates/language/language_item.html:160
msgid "oldest"
msgstr "más antiguo"

#: templates/language/language_item.html:199 templates/widgets/videos.html:93
msgid "Published"
msgstr "Publicado"

#: templates/language/language_item.html:202
#: templates/video/video_item.html:175 templates/widgets/videos.html:96
msgid "views"
msgstr "vistas"

#: templates/playlist/playlist_item_create_edit.html:19
#: templates/settings/settings_index.html:47
#: templates/user/user_playlist_index.html:20
msgid "Playlist"
msgstr ""

#: templates/playlist/playlist_item_create_edit.html:29
#: templates/playlist/playlist_item_create_edit.html:33
msgid "Title"
msgstr "Título"

#: templates/playlist/playlist_item_create_edit.html:37
#: templates/playlist/playlist_item_create_edit.html:41
#: templates/settings/settings_user_update.html:46
msgid "Description"
msgstr "Descripción"

#: templates/playlist/playlist_item_create_edit.html:45
#: templates/user/user_item.html:76 templates/user/user_playlist_index.html:79
#: templates/widgets/playlist.html:63
msgid "Public"
msgstr "Público"

#: templates/playlist/playlist_item_create_edit.html:58
msgid "Update Playlist"
msgstr "Actualizar Playlist"

#: templates/playlist/playlist_item_create_edit.html:60
msgid "Create Playlist"
msgstr "Crear Playlist"

#: templates/settings/settings_index.html:32
msgid "Site"
msgstr "Sitio"

#: templates/settings/settings_index.html:40 templates/widgets/navtabs.html:56
msgid "User"
msgstr "Usuario"

#: templates/settings/settings_site.html:55
msgid "Language"
msgstr "Idioma"

#: templates/settings/settings_site.html:64
msgid "Theme"
msgstr "Tema"

#: templates/settings/settings_site.html:73
msgid "AutoPlay"
msgstr ""

#: templates/settings/settings_site.html:84
msgid "Loop List"
msgstr "Repita a Lista"

#: templates/settings/settings_site.html:95
msgid "Menu"
msgstr "Menú"

#: templates/settings/settings_site.html:116
msgid "Save"
msgstr "Salvar"

#: templates/settings/settings_user_update.html:54
msgid "User Public"
msgstr "Usuario Público"

#: templates/settings/settings_user_update.html:65
msgid "Featured Playlist"
msgstr "Playlist Destacado"

#: templates/settings/settings_user_update.html:70
#: templates/video/video_item.html:89
msgid "Select Playlist"
msgstr "Seleccione Playlist"

#: templates/settings/settings_user_update.html:78
msgid "Update User"
msgstr "Actualizar Usuario"

#: templates/user/user_history_index.html:32
msgid "history"
msgstr "historia"

#: templates/user/user_item.html:50 templates/widgets/playlist.html:92
msgid "History"
msgstr "Historia"

#: templates/user/user_item.html:61
#: templates/user/user_watchlater_index.html:32
#: templates/video/video_item.html:94 templates/widgets/playlist.html:98
msgid "WatchLater"
msgstr "VerDespues"

#: templates/user/user_item.html:79 templates/user/user_playlist_index.html:82
msgid "Private"
msgstr "Privado"

#: templates/video/video_item.html:67
msgid "View on"
msgstr "Ver en"

#: templates/video/video_item.html:71
msgid "Download Torrent"
msgstr "Descargar Torrent"

#: templates/video/video_item.html:100 templates/widgets/videos.html:33
msgid "Add to Playlist"
msgstr "Añadir a Playlist"

#: templates/video/video_item.html:134
msgid "Published on"
msgstr "Publicado en"

#: templates/video/video_item.html:149
msgid "AutoPlay Next Video"
msgstr "AutoPlay Siguiente Video"

#: templates/widgets/navtabs.html:40
msgid "Settings"
msgstr "Configuraciones"

#: templates/widgets/videos.html:46 templates/widgets/videos.html:55
#: templates/widgets/videos.html:70
msgid "Add to WatchLater"
msgstr "Añadir a VerDespues"

#: templates/widgets/widgets_confirm.html:22
msgid "Yes"
msgstr "Sí"

#: templates/widgets/widgets_confirm.html:23
msgid "No"
msgstr ""
